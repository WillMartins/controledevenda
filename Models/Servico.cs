﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendaAPI.Models
{
    public class Servico
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int FuncionarioId { get; set; }
       
        public DateTime Data { get; set; }
        public double ValorTotal { get; set; }

        public ICollection<ProdutoServico> ServicosProdutos { get; set; }

        public Cliente Cliente { get; set; }

        public Funcionario Funcionario { get; set; }

    }
}
